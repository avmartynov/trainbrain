﻿using System.Windows;
using System.Windows.Input;
using Twidlle.Infrastructure;
using Twidlle.Infrastructure.CodeAnnotation;
using Twidlle.Infrastructure.Wpf;
using Twidlle.TrainBrain.Core;

namespace Twidlle.TrainBrain.WpfApp
{
    public partial class MainWindow 
    {
        public MainWindow()
        {
            FormRestoreManager.Initialize(this, App.Settings, s => s.MainWindow);
            InitializeComponent();
            _series = CreateSeries();
        }


        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
            => UpdateUI();


        private void MainWindow_OnUnloaded(object sender, RoutedEventArgs e)
            => _series?.Dispose();


        private void _exitButton_Click(object sender, RoutedEventArgs e)
            => Close();


        private void _checkButton_Click(object sender, RoutedEventArgs e)
        {
            var success = _series.CurrentTask.CheckResultText(_resultTextBox.Text);

            MessageBox.Show(this, 
                _series.GetResultMessage(success), 
                ApplicationInfo.ProductName, MessageBoxButton.OK,
                success ? MessageBoxImage.Information : MessageBoxImage.Error);

            if (success)
            {
                UpdateUI();
                _nextTaskButton.Focus();
            }
        }


        private void _newSeriesButton_Click(object sender, RoutedEventArgs e)
        {
            _series?.Dispose();
            _series = CreateSeries();
            UpdateUI();
        }


        private void _nextTaskButton_Click(object sender, RoutedEventArgs e)
        {
            _series.NextTask();
            UpdateUI();
        }


        private void Window_KeyDown(object sender, [NotNull] KeyEventArgs e) 
        {
            if (e.Key == Key.F1)
                AboutWindow.Show(this, "ApplicationIcon");
        }


        private void UpdateUI()
        {
            Title = $"Task# {_series.TaskCount}";

            _checkButton.IsEnabled   = ! _series.CurrentTask.Finished;
            _resultTextBox.IsEnabled = ! _series.CurrentTask.Finished;
            _taskTextBox.Text        =   _series.CurrentTask.GetTaskText();
            _resultTextBox.Text      = "";

            _resultTextBox.Focus();
        }


        [NotNull]
        private Series CreateSeries()
            => new Series(() => this.InvokeUI(UpdateUI), App.Settings.ShowTaskInterval);

        private Series _series;
    }
}
