﻿using System.Diagnostics;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using Twidlle.Infrastructure;
using Twidlle.Infrastructure.CodeAnnotation;

namespace Twidlle.TrainBrain.WpfApp
{
    public partial class App 
    {
        private void App_OnStartup(object sender, [NotNull] StartupEventArgs e)
        {
            _logger.Info($"Start application with arguments: [{e.Args.Join(", ")}].");
        }


        private void App_OnExit(object sender, [NotNull] ExitEventArgs e)
        {
            Settings.Save();
            _logger.Info($"Exit application with code: {e.ApplicationExitCode}.");            
        }

        private void App_OnDispatcherUnhandledException(object sender, [NotNull] DispatcherUnhandledExceptionEventArgs e)
        {
            _logger.Error($"Fatal error: {e.Exception}");            
        }


        internal static Settings Settings => Settings.Default;

        private static readonly TraceSource _logger = MethodBase.GetCurrentMethod().GetTraceSource();
    }
}
