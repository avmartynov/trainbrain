﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Twidlle.TrainBrain.CmdLineApp.Properties;
using Twidlle.Infrastructure;
using Twidlle.Infrastructure.CodeAnnotation;
using Twidlle.TrainBrain.Core;

namespace Twidlle.TrainBrain.CmdLineApp
{
    public class Program
    {
        public static int Main([NotNull]string[] args)
        {
            var exitCode = 0;
            try
            {
                _logger.Info($"Start '{Path.GetFileNameWithoutExtension(ApplicationInfo.FileName)}'. FilePath = '{ApplicationInfo.FileName}'.  Arguments: [{args.Join(", ")}].");
                _logger.Info($"Settings: {new {Settings.Default.ShowTaskInterval}.ToJson()}.");

                Console.Title = ApplicationInfo.ProductName;

                var series = CreateSeries();
                _logger.Info("New series.");

                ShowNewTask(series);

                for (var line = ""; line != "Q"; line = Console.ReadLine()?.ToUpper() ?? "Q")
                {
                    switch (line)
                    {
                        case "":
                            break;

                        case "N":
                        {
                            series.NextTask();
                            ShowNewTask(series);
                            break;
                        }
                        case "S":
                        {
                            series.Dispose();
                            series = CreateSeries();
                            ShowNewTask(series);
                            break;
                        }
                        default:
                            var success = series.CurrentTask.CheckResultText(line);

                            _logger.Info($"Answer: {line} is {success}.");

                            Console.WriteLine();
                            WriteLine(series.GetResultMessage(success), success ? ConsoleColor.Green : ConsoleColor.Red);
                            Console.WriteLine();

                            Console.WriteLine(_usageMessage);
                            break;
                    }
                }
                _logger.Info($"Exit application with code: {exitCode}.");
            }
            catch (Exception e)
            {
                exitCode = 1;
                _logger.Error($"Fatal error: {e}");            
            }
            return exitCode;
        }


        [NotNull]
        private static Series CreateSeries() 
            => new Series(ClearConsole, Settings.Default.ShowTaskInterval);


        private static void ShowNewTask([NotNull] Series series)
        {
            var task = series.CurrentTask.GetTaskText();

            _logger.Info($"Task: {task}.");
            Console.WriteLine(task);
            Console.WriteLine(_usageMessage);
        }


        private static void ClearConsole()
        {
            Console.Clear();
            Console.WriteLine(_usageMessage);
        }

        private static void WriteLine(string message, ConsoleColor foregroundColor)
        {
            var backgroundColorSave = Console.ForegroundColor;
            Console.ForegroundColor = foregroundColor;
            Console.WriteLine(message);
            Console.ForegroundColor = backgroundColorSave;
        }


        private const string _usageMessage = "Type answer or 'N' for new task  or 'S' for new series or 'Q' for exit application.";

        private static readonly TraceSource _logger = MethodBase.GetCurrentMethod().GetTraceSource();
    }
}
