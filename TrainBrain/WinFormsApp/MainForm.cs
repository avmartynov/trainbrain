﻿using System;
using System.Windows.Forms;
using Twidlle.Infrastructure;
using Twidlle.Infrastructure.CodeAnnotation;
using Twidlle.Infrastructure.WinForms;
using Twidlle.TrainBrain.Core;
using Twidlle.TrainBrain.WinFormsApp.Properties;

namespace Twidlle.TrainBrain.WinFormsApp
{
    public partial class MainForm : Form
	{
		public MainForm()
		{
            FormRestoreManager.Initialize(this, Settings.Default, s => s.MainForm);
			InitializeComponent();
		    _series = CreateSeries();
		}


	    private void MainForm_Load(object sender, EventArgs e)
	        => UpdateUI();


	    private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
	        => _series?.Dispose();


	    private void newSeriesButton_Click(object sender, EventArgs e)
	    {
	        _series?.Dispose();
	        _series = CreateSeries();
	        UpdateUI();
	    }


		private void newTaskButton_Click(object sender, EventArgs e)
		{
			_series.NextTask();
		    UpdateUI();
		}


	    private void checkButton_Click(object sender, EventArgs e)
	    {
	        var success = _series.CurrentTask.CheckResultText(_resultTextBox.Text);

	        MessageBox.Show(this, 
	            _series.GetResultMessage(success), 
	            ApplicationInfo.ProductName, MessageBoxButtons.OK,
	            success ? MessageBoxIcon.Information : MessageBoxIcon.Error);

	        if (success)
	        {
	            UpdateUI();
	            _newTaskButton.Focus();
	        }
	    }


	    private void MainForm_KeyDown(object sender, [NotNull] KeyEventArgs e)
	    {
	        // Важно: this.KeyPreview = true 
	        if (e.KeyCode == Keys.F1)
	            new AboutForm(Resources.ApplicationIcon).ShowDialog();
	    }


	    private void exitButton_Click(object sender, EventArgs e)
	        => Close();


	    [NotNull]
        private Series CreateSeries()
            => new Series(() => this.InvokeUI(UpdateUI), Settings.Default.ShowTaskInterval);


	    private void UpdateUI()
	    {
	        Text = $"Task# {_series.TaskCount}";

	        _checkButton.Enabled   = ! _series.CurrentTask.Finished;
	        _resultTextBox.Enabled = ! _series.CurrentTask.Finished;
	        _taskLabel.Text        = _series.CurrentTask.GetTaskText();
	        _resultTextBox.Text    = null;

	        _resultTextBox.Focus();
	    }


	    private Series _series;
    }
}