﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Twidlle.Infrastructure;
using Twidlle.Infrastructure.WinForms;
using Twidlle.TrainBrain.WinFormsApp.Properties;

namespace Twidlle.TrainBrain.WinFormsApp
{
    public sealed class Program : IDisposable
	{
	    /// <summary>
	    /// The main entry point for the application.
	    /// </summary>
	    [STAThread]
	    private static void Main(string[] args)
	    {
	        try
	        {
	            _logger.Info($"Start '{Application.ProductName}'. FilePath = '{Application.ExecutablePath}'.  Arguments: [{args.Join(", ")}].");
	            _logger.Info($"Settings: {new {Settings.Default.MainForm, Settings.Default.ShowTaskInterval}.ToJson()}.");

	            _logger.Info("Compose application.");
	            var program = new Program();

	            _logger.Info("Run application.");
	            program.Run();

	            _logger.Info($"Application successfully finished.{Environment.NewLine}");
	        }
	        catch (Exception exception)
	        {
	            _logger.Error(exception, "Application failed.");

	            exception.ShowMessageBox();
	        }
	        finally
	        {
	            _logger.Info($"Application finished.{Environment.NewLine}");
	        }
	    }


	    public Program()
	    {
	        Application.EnableVisualStyles();
	        Application.SetCompatibleTextRenderingDefault(false);
	        Application.ThreadException += ApplicationOnThreadException;

	        _mainForm = new MainForm();
	    }


	    private void Run()
	    {
	        Application.Run(_mainForm);
	        Settings.Default.Save();
	    }


	    private void ApplicationOnThreadException(object sender, ThreadExceptionEventArgs args)
	    {
	        _logger.Error(args.Exception);
	        args.Exception.ShowMessageBox(_mainForm);

	        _logger.Error($"Application exit. Exit code: {FAIL_EXIT_CODE}.");
	        Environment.Exit(FAIL_EXIT_CODE);
	    }


	    public void Dispose()
	    {
	        _logger.Error("Main form dispose.");
	        _mainForm.Dispose();
	    }


	    private readonly Form _mainForm;

	    private static readonly TraceSource _logger =  MethodBase.GetCurrentMethod().GetTraceSource();

	    private const int FAIL_EXIT_CODE = 1;
	}
}
