﻿namespace Twidlle.TrainBrain.WinFormsApp
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this._exitButton = new System.Windows.Forms.Button();
            this._checkButton = new System.Windows.Forms.Button();
            this._newTaskButton = new System.Windows.Forms.Button();
            this._resultTextBox = new System.Windows.Forms.TextBox();
            this._taskLabel = new System.Windows.Forms.Label();
            this._newSeriesButton = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // _exitButton
            // 
            this._exitButton.Location = new System.Drawing.Point(93, 135);
            this._exitButton.Name = "_exitButton";
            this._exitButton.Size = new System.Drawing.Size(75, 23);
            this._exitButton.TabIndex = 5;
            this._exitButton.Text = "Exit";
            this._exitButton.UseVisualStyleBackColor = true;
            this._exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // _checkButton
            // 
            this._checkButton.Location = new System.Drawing.Point(12, 135);
            this._checkButton.Name = "_checkButton";
            this._checkButton.Size = new System.Drawing.Size(75, 23);
            this._checkButton.TabIndex = 3;
            this._checkButton.Text = "Check Result";
            this._checkButton.UseVisualStyleBackColor = true;
            this._checkButton.Click += new System.EventHandler(this.checkButton_Click);
            // 
            // _newTaskButton
            // 
            this._newTaskButton.Location = new System.Drawing.Point(12, 106);
            this._newTaskButton.Name = "_newTaskButton";
            this._newTaskButton.Size = new System.Drawing.Size(75, 23);
            this._newTaskButton.TabIndex = 2;
            this._newTaskButton.Text = "Next Task";
            this._newTaskButton.UseVisualStyleBackColor = true;
            this._newTaskButton.Click += new System.EventHandler(this.newTaskButton_Click);
            // 
            // _resultTextBox
            // 
            this._resultTextBox.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resultTextBox.Location = new System.Drawing.Point(12, 60);
            this._resultTextBox.Name = "_resultTextBox";
            this._resultTextBox.Size = new System.Drawing.Size(156, 33);
            this._resultTextBox.TabIndex = 1;
            this._resultTextBox.Text = "7945";
            this._resultTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._toolTip.SetToolTip(this._resultTextBox, "Type here result");
            // 
            // _taskLabel
            // 
            this._taskLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._taskLabel.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._taskLabel.Location = new System.Drawing.Point(12, 14);
            this._taskLabel.Name = "_taskLabel";
            this._taskLabel.Size = new System.Drawing.Size(156, 33);
            this._taskLabel.TabIndex = 0;
            this._taskLabel.Text = "69 x 73";
            this._taskLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._toolTip.SetToolTip(this._taskLabel, "Task");
            // 
            // _newSeriesButton
            // 
            this._newSeriesButton.Location = new System.Drawing.Point(93, 106);
            this._newSeriesButton.Name = "_newSeriesButton";
            this._newSeriesButton.Size = new System.Drawing.Size(75, 23);
            this._newSeriesButton.TabIndex = 4;
            this._newSeriesButton.Text = "New Series";
            this._newSeriesButton.UseVisualStyleBackColor = true;
            this._newSeriesButton.Click += new System.EventHandler(this.newSeriesButton_Click);
            // 
            // MainForm
            // 
            this.AcceptButton = this._checkButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(182, 167);
            this.Controls.Add(this._taskLabel);
            this.Controls.Add(this._resultTextBox);
            this.Controls.Add(this._newTaskButton);
            this.Controls.Add(this._checkButton);
            this.Controls.Add(this._newSeriesButton);
            this.Controls.Add(this._exitButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Train Your Brain";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button _exitButton;
		private System.Windows.Forms.Button _checkButton;
		private System.Windows.Forms.Button _newTaskButton;
		private System.Windows.Forms.TextBox _resultTextBox;
		private System.Windows.Forms.Label _taskLabel;
		private System.Windows.Forms.Button _newSeriesButton;
        private System.Windows.Forms.ToolTip _toolTip;
    }
}

