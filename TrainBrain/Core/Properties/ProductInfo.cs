using System;

// ReSharper disable once CheckNamespace
namespace Twidlle.TrainBrain
{
    public class ProductInfo
    {
        public const string Name = "Twidlle Train Brain";
        public const string Version = "2.0.10.*";
        public const string Culture = "";
        public const string Year = "2018";
        public const string Copyright = "Copyright © " + Year + " " + CompanyInfo.Name;

    #if DEBUG
        public const String Configuration = "Debug";
    #else
        public const String Configuration = "Release";
    #endif
    }
}
