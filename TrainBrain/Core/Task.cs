﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Twidlle.Infrastructure.CodeAnnotation;

namespace Twidlle.TrainBrain.Core
{
    public delegate void HideTaskCallback();

	public sealed class Task : IDisposable
	{
	    public Task([NotNull] HideTaskCallback hideCallback, TimeSpan hideDueTime)
	    {
	        _hideTimer = new Timer(s =>
	                               {
	                                   _visible = false;
	                                   hideCallback();
	                               }, this, hideDueTime, Timeout.InfiniteTimeSpan);
	    }

	    public int Left  => _pair.Left;

	    public int Right => _pair.Right;

		public bool Finished => !_stopwatch.IsRunning;

	    public bool Visible  => _visible;

	    public TimeSpan Duration => _stopwatch.Elapsed;


		public bool CheckResult(int result)
		{
		    if (Left * Right != result)
		        return false;
			
			_stopwatch.Stop();
			return true;
		}


	    public bool CheckResultText(string resultText)
	        => int.TryParse(resultText, out var result) && CheckResult(result);


        public void Dispose() 
	        => _hideTimer?.Dispose();


	    private class NumberPair
	    {
	        public NumberPair()
	        {
	            Left  = GenerateUniqueDigit() * 10 + GenerateUniqueDigit();
	            Right = GenerateUniqueDigit() * 10 + GenerateUniqueDigit();
	        }

	        public int Left  { get; }
	        public int Right { get; }


	        private int GenerateUniqueDigit()
	        {
	            int r;
	            while (_usedDigits.IndexOf(r = _random.Next(2, 9)) != -1)
	            {
	            }
	            _usedDigits.Add(r);
	            return r;
	        }
        
	        private readonly Random    _random = new Random((int)DateTime.Now.Ticks);
	        private readonly List<int> _usedDigits = new List<int>();
	    }

	    private bool _visible =  true;

	    private readonly Timer      _hideTimer;
	    private readonly NumberPair _pair      = new NumberPair();
		private readonly Stopwatch  _stopwatch = Stopwatch.StartNew();
	}
}
