﻿using System;
using Twidlle.Infrastructure.CodeAnnotation;

namespace Twidlle.TrainBrain.Core
{
	public sealed class Series : IDisposable
	{
	    public Series([NotNull] HideTaskCallback hideTaskCallback, TimeSpan hideDueTime)
	    {
	        _hideDueTime = hideDueTime;
	        _hideTaskCallback = hideTaskCallback ?? throw new ArgumentNullException(nameof(hideTaskCallback));
	        _task = new Task(_hideTaskCallback, _hideDueTime);
	        _taskCount = 1;
	    }

	    public void Dispose()
	        => _task.Dispose();

	    public Task     CurrentTask     => _task;
	    public int      TaskCount       => _taskCount;
		public TimeSpan Duration        => DateTime.Now - _startDate;
	    public TimeSpan AverageTaskTime => TimeSpan.FromMilliseconds(Duration.TotalMilliseconds / TaskCount);

		public void NextTask()
		{
		    if (_task.Finished)
		        _taskCount++;

		    _task.Dispose();
		    _task = new Task(_hideTaskCallback, _hideDueTime);
		}


        private int  _taskCount;
	    private Task _task;

	    private readonly TimeSpan _hideDueTime;
	    private readonly HideTaskCallback _hideTaskCallback;
	    private readonly DateTime _startDate = DateTime.Now;
	}
}
