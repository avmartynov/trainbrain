﻿using Twidlle.Infrastructure.CodeAnnotation;

namespace Twidlle.TrainBrain.Core
{
    public static class TaskPresentation
    {
        [NotNull]
        public static string GetResultMessage([NotNull] this Series series, bool success)
            => success ? GetTrueResultMessage(series) : GetWrongResultMessage(series);


        [NotNull]
        public static string GetTaskText([NotNull] this Task task)
            => task.Visible ? $"{task.Left} x {task.Right}" : "";


        [NotNull]
        internal static string GetTrueResultMessage([NotNull] this Series series)
            => "TRUE\n\n" +
               $"Test#: {series.TaskCount}, Duration: {series.CurrentTask.Duration:mm\\:ss}\n\n" +
               $"Avr. test duration: {series.AverageTaskTime:mm\\:ss}";


        [NotNull]
        internal static string GetWrongResultMessage([NotNull] this Series series)
            => "W R O N G";
    }
}
